#!/usr/bin/env bash

set -e

KEY_SUFFIX=${KEY_SUFFIX^^}

for key in $*
do
  value=${!key}
  echo "$key${KEY_SUFFIX}=$value"
done
