ARG GO_MAJOR_MINOR=1.19
ARG GO_VERSION=${GO_MAJOR_MINOR}.4

# Multiplatform build described in blog https://www.docker.com/blog/faster-multi-platform-builds-dockerfile-cross-compilation-guide/
FROM --platform=$BUILDPLATFORM golang:$GO_VERSION
ARG TARGETOS
ARG TARGETARCH
ENV CGO_ENABLED=0
WORKDIR /go/builds
COPY cmd/caddy/main.go go.mod go.sum ./
RUN go mod download
RUN --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    GOOS=$TARGETOS GOARCH=$TARGETARCH go build -o caddy

ENTRYPOINT []
CMD ["caddy"]
